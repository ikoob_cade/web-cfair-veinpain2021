export const environment = {
  production: true,
  base_url: 'https://d3v-server-cf-air-api.ikoob.co.kr/api/v1',
  socket_url: 'https://d3v-server-cf-air-socket-api.ikoob.co.kr',
  eventId: '6193212ca7ff7a001384dfd9' // Veinpain2021 Dev
};
