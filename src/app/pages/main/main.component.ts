import * as _ from 'lodash';
import { _ParseAST } from '@angular/compiler';
import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { SpeakerService } from '../../services/api/speaker.service';
import { BoothService } from '../../services/api/booth.service';
import { CookieService } from 'ngx-cookie-service';
import { CategoryService } from '../../services/api/category.service';
import { FunctionService } from '../../services/function/function.service';
import { EventService } from '../../services/api/event.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DayjsService } from '../../services/dayjs.service';
import { environment } from '../../../environments/environment';

declare var $: any;

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, AfterViewInit {
  @ViewChild('entryAlertBtn') entryAlert: ElementRef;
  public speakers: Array<any> = [];
  public sponsors: Array<any> = []; // 스폰서 목록
  public selectedBooth: any;
  public mobile: boolean;

  public programBookUrl = '';
  public excerptUrl = '';

  public user;
  public attachments = []; // 부스 첨부파일
  public boothCategories: Array<any> = []; // 카테고리[부스] 목록
  event;

  constructor(
    private speakerService: SpeakerService,
    private boothService: BoothService,
    private cookieService: CookieService,
    private categoryService: CategoryService,
    private functionService: FunctionService,
    private eventService: EventService,
    private activatedRoute: ActivatedRoute,
    private dayjsService: DayjsService,
    private router: Router,
  ) {
  }


  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    // if (window.innerWidth < 768 !== this.mobile) {
    //   this.loadSpeakers();
    // }
  }

  ngOnInit(): void {
    this.user = sessionStorage.getItem('cfair');
    this.loadSpeakers();
    this.loadBooths();
    this.checkEventVersion();
  }

  speakerLoaded = false;
  speakerDetail;

  getSpeakerDetail(speaker): void {
    setTimeout(() => {
      this.speakerLoaded = false;
    }, 0);

    if (this.user) {
      this.speakerService.findOne(speaker.id).subscribe(res => {
        this.speakerDetail = res;
        this.speakerLoaded = true;
        $('#speakersModal').modal('show');
      });
    } else {
      this.router.navigate(['/login']);
    }
  }


  // 버전확인
  checkEventVersion(): void {
    this.eventService.findOne().subscribe(res => {
      this.event = res;
      if (this.event.clientVersion) {
        const clientVersion = localStorage.getItem(this.event.eventCode + 'ver');
        if (!clientVersion) {
          localStorage.setItem(this.event.eventCode + 'ver', this.event.clientVersion);
          location.reload();
        } else if (clientVersion) {
          if (clientVersion !== this.event.clientVersion) {
            localStorage.setItem(this.event.eventCode + 'ver', this.event.clientVersion);
            location.reload();
          }
        }
      }
    });
  }

  ngAfterViewInit(): void {
    if (this.cookieService.get(`${environment.eventId}_main_popup`)) {
      let cookieTime = this.cookieService.get(`${environment.eventId}_main_popup`);
      const now = new Date().getTime().toString();
      if (cookieTime < now) {
        this.cookieService.delete(`${environment.eventId}_main_popup`);
        this.entryAlert.nativeElement.click();
      }
    } else {
    // const first = this.dayjsService.makeTime('2021-11-05 20:00');
    // const end = this.dayjsService.makeTime('2021-11-05 20:10');
    // const now = this.dayjsService.makeTime();

    // if (now.isSameOrAfter(first) && now.isSameOrBefore(end)) {
      // ! 메인 팝업
      setTimeout(() => {
        this.entryAlert.nativeElement.click();
      }, 0);
    // }
    }
  }

  noModal(): void {
    this.cookieService.set(`${environment.eventId}_main_popup`, 'true', { expires: new Date(new Date().getTime() + 1000 * 60 * 60) });
  }

  loadBooths = () => {
    this.boothService.find().subscribe(res => {
      const categories =
        _.chain(res)
          .groupBy(booth => {
            return booth.category ? JSON.stringify(booth.category) : '{}';
          })
          .map((booth, category) => {
            category = JSON.parse(category);
            category.booths = booth;

            return category;
          }).sortBy(category => {
            return category.seq;
          })
          .value();

      this.boothCategories = categories;
    });
  }

  // 발표자 리스트를 조회한다.
  loadSpeakers(): void {
    const limit = 6;
    this.speakerService.find(false, true).subscribe(res => {
      this.speakers = this.functionService.division(res, limit);
    });
  }

}
