import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EBoothComponent } from './e-booth.component';

describe('EBoothComponent', () => {
  let component: EBoothComponent;
  let fixture: ComponentFixture<EBoothComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EBoothComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EBoothComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
