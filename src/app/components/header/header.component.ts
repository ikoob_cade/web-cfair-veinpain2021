import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { MemberService } from '../../services/api/member.service';
import { AuthService } from '../../services/auth/auth.service';
import { EventService } from '../../services/api/event.service';
import { SocketService } from '../../services/socket/socket.service';
import { FunctionService } from '../../services/function/function.service';

declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  public user: any;
  public menus: any = [
    { title: 'Welcome', router: '/about' },
    { title: 'Program', router: '/program' },
    // { title: 'LIVE', router: '/live' },
    { title: 'Lectures', router: '/vod' },
    { title: 'Speakers', router: '/speakers' },
    // { title: 'e-Posters', router: '/posters' },
    // { title: 'Sponsor', router: '/e-booth' },
    { title: 'Notice', router: '/board' },
  ];

  collapse = true;

  event;

  constructor(
    private router: Router,
    private eventService: EventService,
    private authService: AuthService,
    private memberService: MemberService,
    private socketService: SocketService,
    private functionService: FunctionService
  ) {
    router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((val) => {
        const auth = JSON.parse(sessionStorage.getItem('cfair'));
        this.user = (auth && auth.token) ? auth : undefined;
        if (this.user) {
          this.loginSocket();
        }

        $('.navbar-collapse').collapse('hide');
        this.collapse = true;

        this.functionService.scrollToTop();
      });
  }

  ngOnInit(): void {
    // this.checkEventVersion();
    this.eventService.findOne().subscribe(res => this.event = res);
  }

  // 버전확인
  checkEventVersion(): void {
    this.eventService.findOne().subscribe(res => {
      this.event = res;
      if (this.event.clientVersion) {
        const clientVersion = localStorage.getItem(this.event.eventCode + 'ver');
        if (!clientVersion) {
          localStorage.setItem(this.event.eventCode + 'ver', this.event.clientVersion);
          location.reload();
        } else if (clientVersion) {
          if (clientVersion !== this.event.clientVersion) {
            localStorage.setItem(this.event.eventCode + 'ver', this.event.clientVersion);
            location.reload();
          }
        }
      }
    });
  }


  ngOnDestroy(): void {
  }

  postAttendance = (): void => {
    this.memberService.attendance(this.user.id).subscribe(res => { });
  }

  /** 로그아웃 */
  logout(): void {
    this.authService.logout().subscribe(res => {
      this.logoutSocket();

      sessionStorage.removeItem('cfair');
      this.router.navigate(['/']);
    }, error => {
      sessionStorage.removeItem('cfair');
      this.router.navigate(['/']);
    });
  }

  loginSocket() {
    this.socketService.login({
      memberId: this.user.id,
      token: this.user.token
    });
  }

  logoutSocket() {
    this.socketService.logout({
      memberId: this.user.id
    });
  }

  openKakao(): void {
    window.open('http://pf.kakao.com/_rxnSfs/chat', '_blank');
  }

}
